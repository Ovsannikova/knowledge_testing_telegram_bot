<?php
session_start();
@$old_user = $_SESSION['valid_user'];

unset($_SESSION['valid_user']);

$result_dest = session_destroy();

if (!empty($old_user)) {

	if ($result_dest) {
		echo "Successful logout!";
		header('Refresh: 3; URL=index.html');
	}

	else {
		echo "Exiting the system is impossible.";
	}
}

else {
	//если пользователь не входил в систему
	//но каким-то чудом попал на эту страницу
	echo "You are not logged on.";
	echo "<br>";
	echo "<a href='index.html'>Start page</a>";
}

?>
-- phpMyAdmin SQL Dump
-- version 4.7.3
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Час створення: Трв 30 2019 р., 10:00
-- Версія сервера: 5.7.19
-- Версія PHP: 7.0.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База даних: `zdtutestsdb`
--

-- --------------------------------------------------------

--
-- Структура таблиці `current_test`
--

CREATE TABLE `current_test` (
  `id` int(11) NOT NULL,
  `test_table_name` varchar(100) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `current_test`
--

INSERT INTO `current_test` (`id`, `test_table_name`, `user_id`) VALUES
(1, 'anna', 1);

-- --------------------------------------------------------

--
-- Структура таблиці `groups`
--

CREATE TABLE `groups` (
  `id` int(11) NOT NULL,
  `group_name` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `groups`
--

INSERT INTO `groups` (`id`, `group_name`) VALUES
(1, 'Pi50'),
(2, 'ZPIK-17'),
(3, 'ZPIK-18');

-- --------------------------------------------------------

--
-- Структура таблиці `ques_sl`
--

CREATE TABLE `ques_sl` (
  `id` int(11) NOT NULL,
  `question` varchar(200) NOT NULL,
  `answer` varchar(200) NOT NULL,
  `zap` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `ques_sl`
--

INSERT INTO `ques_sl` (`id`, `question`, `answer`, `zap`) VALUES
(1, 'как дела?|1.плохо| 2. хорошо| 3ютак себе| 4. умер', '1', '1.плохо; 2. хорошо; 3ютак себе; 4. умер'),
(2, 'крокодил?|1.хороший|2.злой|3.коварный|4.тупой', '2', '1.плохо; 2. хорошо; 3ютак себе; 4. умер'),
(3, 'How created table|1.table|2.tbody|3.thead|4.theader', '1', NULL);

-- --------------------------------------------------------

--
-- Структура таблиці `ques_sl1`
--

CREATE TABLE `ques_sl1` (
  `id` int(11) NOT NULL,
  `question` varchar(200) NOT NULL,
  `answer` varchar(200) NOT NULL,
  `zap` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `ques_sl1`
--

INSERT INTO `ques_sl1` (`id`, `question`, `answer`, `zap`) VALUES
(1, 'Hwo eat rabbit?|1.плохо| 2. хорошо| 3ютак себе| 4. умер', '1', '1.плохо; 2. хорошо; 3ютак себе; 4. умер'),
(3, 'Where find phone| 1.in table| 2. in sky| 3. in Odessa| 4.in bed', '2', NULL),
(4, 'Что такое таракан?| 1.Птица| 2. Рыба| 3. Насекомое| 4.Животное', '3', NULL);

-- --------------------------------------------------------

--
-- Структура таблиці `results_table`
--

CREATE TABLE `results_table` (
  `id` int(11) NOT NULL,
  `test_name` varchar(200) NOT NULL,
  `test_points` int(11) NOT NULL,
  `test_rating` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `results_table`
--

INSERT INTO `results_table` (`id`, `test_name`, `test_points`, `test_rating`, `user_id`, `date`) VALUES
(1, 'FIRST_LEVEL_TEST', 1, 2, 381407062, '2019-05-24'),
(2, 'SECOND_LEVEL_TEST', 1, 2, 381407062, '2019-05-24'),
(3, 'FIRST_LEVEL_TEST', 0, 2, 381407062, '2019-05-24'),
(4, 'FIRST_LEVEL_TEST', 2, 5, 647375763, '2019-05-24'),
(5, 'SECOND_LEVEL_TEST', 1, 2, 647375763, '2019-05-24'),
(6, 'FIRST_LEVEL_TEST', 1, 2, 381407062, '2019-05-24'),
(7, 'SECOND_LEVEL_TEST', 0, 2, 381407062, '2019-05-24'),
(8, 'FIRST_LEVEL_TEST', 0, 2, 625471755, '2019-05-28'),
(9, 'FIRST_LEVEL_TEST', 0, 2, 517324437, '2019-05-28'),
(10, 'SECOND_LEVEL_TEST', 0, 2, 517324437, '2019-05-28'),
(11, 'FIRST_LEVEL_TEST', 0, 2, 517324437, '2019-05-28'),
(12, 'FIRST_LEVEL_TEST', 1, 2, 625471755, '2019-05-28'),
(13, 'FIRST_LEVEL_TEST', 2, 3, 625471755, '2019-05-28'),
(14, 'SECOND_LEVEL_TEST', 0, 2, 625471755, '2019-05-28'),
(15, 'FIRST_LEVEL_TEST', 0, 2, 625471755, '2019-05-28'),
(16, 'FIRST_LEVEL_TEST', 0, 2, 625471755, '2019-05-28'),
(17, 'SECOND_LEVEL_TEST', 0, 2, 625471755, '2019-05-28'),
(18, 'FIRST_LEVEL_TEST', 0, 2, 625471755, '2019-05-28'),
(19, 'FIRST_LEVEL_TEST', 1, 2, 625471755, '2019-05-28'),
(20, 'FIRST_LEVEL_TEST', 1, 2, 625471755, '2019-05-28'),
(21, 'FIRST_LEVEL_TEST', 0, 2, 625471755, '2019-05-29'),
(22, 'SECOND_LEVEL_TEST', 0, 2, 625471755, '2019-05-29'),
(23, 'FIRST_LEVEL_TEST', 0, 2, 625471755, '2019-05-29');

-- --------------------------------------------------------

--
-- Структура таблиці `test_id`
--

CREATE TABLE `test_id` (
  `id` int(11) NOT NULL,
  `title_test` varchar(200) NOT NULL,
  `description` text NOT NULL,
  `table_name` varchar(10) NOT NULL,
  `bot_command` text NOT NULL,
  `level` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `test_id`
--

INSERT INTO `test_id` (`id`, `title_test`, `description`, `table_name`, `bot_command`, `level`) VALUES
(1, 'Test1', 'rjtgnjne', 'ques_sl', '/start', 1),
(2, 'menu\r\n', '', '', '/menu', 1),
(3, 'test2', '', 'ques_sl1', '2', 2);

-- --------------------------------------------------------

--
-- Структура таблиці `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `first_name` varchar(200) NOT NULL,
  `last_name` varchar(200) DEFAULT NULL,
  `role` int(11) NOT NULL,
  `password` int(11) DEFAULT NULL,
  `group_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `users`
--

INSERT INTO `users` (`id`, `user_id`, `first_name`, `last_name`, `role`, `password`, `group_id`) VALUES
(1, 1, 'ann', 'ann', 1, 1, 1),
(3, 381407062, 'Анастасия', '', 3, NULL, 1),
(4, 647375763, 'Dan', 'Ann', 3, NULL, 1),
(5, 625471755, 'Anna', 'Dan', 3, NULL, 1),
(6, 517324437, 'Polina', '', 3, NULL, 2);

--
-- Індекси збережених таблиць
--

--
-- Індекси таблиці `current_test`
--
ALTER TABLE `current_test`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Індекси таблиці `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`id`);

--
-- Індекси таблиці `ques_sl`
--
ALTER TABLE `ques_sl`
  ADD PRIMARY KEY (`id`);

--
-- Індекси таблиці `ques_sl1`
--
ALTER TABLE `ques_sl1`
  ADD PRIMARY KEY (`id`);

--
-- Індекси таблиці `results_table`
--
ALTER TABLE `results_table`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Індекси таблиці `test_id`
--
ALTER TABLE `test_id`
  ADD PRIMARY KEY (`id`);

--
-- Індекси таблиці `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `group_id` (`group_id`);

--
-- AUTO_INCREMENT для збережених таблиць
--

--
-- AUTO_INCREMENT для таблиці `current_test`
--
ALTER TABLE `current_test`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT для таблиці `groups`
--
ALTER TABLE `groups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT для таблиці `ques_sl`
--
ALTER TABLE `ques_sl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT для таблиці `ques_sl1`
--
ALTER TABLE `ques_sl1`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT для таблиці `results_table`
--
ALTER TABLE `results_table`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT для таблиці `test_id`
--
ALTER TABLE `test_id`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT для таблиці `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

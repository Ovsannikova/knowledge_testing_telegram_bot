<?php
include 'db_fns.php';
//========================== LOGIN ===========================
function login($last_name, $password) {
	$conn = db_connect();
	$result = $conn->query(" SELECT * FROM users WHERE last_name='$last_name' AND password='$password' ");

	if (!$result)
		throw new Exception("Can not log in. Please, try again later.");

	if ($result->num_rows > 0)
		return true;
	else
		throw new Exception("Can not log in. Please, try again later.");
}

//========================= GET ROLE ==========================
function get_role($last_name) {
	$role = NULL;	

	$conn = db_connect();
	$result = $conn->query(" SELECT role FROM users WHERE last_name='$last_name' ");

	$row = $result->fetch_row();
    $role = $row[0];

    return $role;
}
//======================== CHECK USER =========================
function check_valid_user() {
	global $valid_user;
	$flag = true;	//true - значит все ОК
	
	if (isset($_SESSION['valid_user'])) 
		$flag = true;
	else 
		$flag = false;	
	
	return $flag;
}


//====================== GENERATE PASSWORD =====================
function generate_pass($length_pass){
	$chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
	$numChars = strlen($chars);
	$new_pass = '';	//новый пароль
	
	for ($i = 0; $i < $length_pass; $i++)
		$new_pass .= substr($chars, rand(1, $numChars) - 1, 1);

	return $new_pass;
}

?>
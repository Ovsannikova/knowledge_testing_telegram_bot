<?php
require_once("admin_auth_fns.php");
require_once("output_fns.php");

session_start();

@$login = $_POST['login'];
@$password = $_POST['password'];

if ($login && $password) {
    try {
        login($login, $password);
        $_SESSION['valid_user'] = $login;
    }
    catch (Exception $e) {
        echo "Invalid login or password.";
        exit();
    }
}

if (check_valid_user()) {
    try {
        if (get_role($login) == 1) {
            echo "Welcome, " . $_SESSION['valid_user'] . " to the admin panel! | ";
            include "main.php";
            echo "<a href='adminpanel_logout.php'>LOGOUT</a>";
        }

        if (get_role($login) == 2) {
            echo "Welcome, " . $_SESSION['valid_user'] . " to the moderator panel! | ";
            echo "<a href='adminpanel_logout.php'>LOGOUT</a>";
        }

        if (get_role($login) == 3)
            echo "You are not allowed access to the admin panel.<br>Go away from this page.";
    }
    catch (Exception $e) {
        echo "Some problem with DB. Please, try again later.<br>";
        echo "Details: " . $e;
        exit();
    }
}

else {
    echo "You are not logged in.";
}


?>